package com.tnt.templatemessage.demo;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description
 * @Author zhuangxl
 * @Date 2021-01-15 15:01
 */
public class MyTemplateMessage {

    public static void main(String[] args) {
        try {

            String access_token = WechatUtil.getAccessionToken();

            /**----------------------无参数模板--------------------------*/
            Map<String, Object> paramsMap = new HashMap<>();
            paramsMap.put("touser", "ouW9KtxDXuT_mGNKPLhOtjcOntWE");
            paramsMap.put("template_id", "wJ97mdVBfUA9ZCZelkNTkQsgR_of0nUinswQ8tyNh_w");

            WechatUtil.sendTemplateMessage(access_token, paramsMap);

            /**----------------------有参数模板--------------------------*/
            paramsMap = new HashMap<>();
            paramsMap.put("touser", "ouW9KtxDXuT_mGNKPLhOtjcOntWE");
            paramsMap.put("template_id", "Nv6uPI5duU6X5W_TTTsE1WC6n5QAAP1DJ812wt07ZTI");

            //数据参数
            Map<String, Object> dataMap = new HashMap<>();
            dataMap.put("first", new DataVo("恭喜你购买成功！", "#173177"));
            dataMap.put("keynote1", new DataVo("巧克力", "#173177"));
            dataMap.put("keynote2", new DataVo("39.8元", "#173177"));
            dataMap.put("keynote3", new DataVo("2014年9月22日", "#173177"));
            dataMap.put("remark", new DataVo("欢迎再次购买!", "#173177"));

            paramsMap.put("data", dataMap);

            //跳转参数: URL地址（链接需要用开发回调的域名） 或者 微信小程序地址（与公众号关联的）
            paramsMap.put("url", "http://weixin.qq.com/download");

            WechatUtil.sendTemplateMessage(access_token, paramsMap);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
