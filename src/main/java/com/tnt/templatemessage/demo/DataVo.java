package com.tnt.templatemessage.demo;

/**
 * @Description
 * @Author zhuangxl
 * @Date 2021-01-15 21:12
 */
public class DataVo {

    private String value;
    private String color;

    public DataVo(String value, String color) {
        this.value = value;
        this.color = color;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
