package com.tnt.templatemessage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TemplateMessageApplication {

	public static void main(String[] args) {
		SpringApplication.run(TemplateMessageApplication.class, args);
	}

}
