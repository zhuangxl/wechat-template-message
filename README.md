# wechat-template-message

#### 介绍
微信公众号-服务号模板消息

#### 微信公众号接口测试账号
http://mp.weixin.qq.com/debug/cgi-bin/sandbox?t=sandbox/login

用自己的微信扫描登录
获取access token 和 发送模板消息不需要配置接口地址。

关注测试账号，然后配置模板。复制模板ID替换程序中的模板ID即可。（appid 和 appsecret当然也要替换）

##### 模板标题：
模板消息测试示例
##### 模板内容：
first: {{first.DATA}} keynote1:{{keynote1.DATA}} keynote2:{{keynote2.DATA}} keynote3:{{keynote3.DATA}} remark:{{remark.DATA}}
##### 
![图示](https://gitee.com/zhuangxl/wechat-template-message/raw/master/src/main/resources/docimg/1122.png)
